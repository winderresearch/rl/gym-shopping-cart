import gym
import gym_shopping_cart
import numpy as np
import logging
import sys

logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

env = gym.make("ShoppingCart-v0")
episode_over = False
chicken_id = 6046
print(env.data.columns()[chicken_id])
print(env.data.product_str(chicken_id))
action = np.zeros(env.data.n_products())
action[chicken_id] = 1
rewards = 0
while not episode_over:
    state, reward, episode_over, _ = env.step(action)
    print(state, reward)
    rewards += reward
print("Total reward: {}".format(rewards))
