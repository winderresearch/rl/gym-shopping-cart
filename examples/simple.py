import gym
import gym_shopping_cart

env = gym.make("ShoppingCart-v0")
episode_over = False
rewards = 0
while not episode_over:
    state, reward, episode_over, _ = env.step(env.action_space.sample())
    print(state, reward)
    rewards += reward
print("Total reward: {}".format(rewards))
